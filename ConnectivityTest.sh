#!/bin/bash
echo "                                      "
echo "by:                                   "
echo " _____         _           _ _        "
echo "|   __|___ ___| |_ _ _ ___|_| |_ _ _  "
echo "|__   | -_| -_| '_| | |  _| |  _| | | "
echo "|_____|___|___|_,_|___|_| |_|_| |_  | "
echo "                                |___| "

time=$(date '+%d/%m/%Y %H:%M:%S');
start=0

target=google.com

echo -e "\n Write the duration of the test in minutes: "
read duration

echo "Set for $duration minutes!"

while [ $start -lt $duration ]; do 
	if ping -q -c 1 -W 1 $target >/dev/null; then
  		echo -e '[ OK ] Connectivity' >> ConnectivityReport.txt
	else
  		echo -e '[ KO ] No Connectivity - ' $time >> ConnectivityReport.txt
  		echo '[ * ] Network error!'
  		traceroute -I $target >> ConnectivityReport.txt
	fi
	min_left=$(($duration-$start))
	echo "[ $min_left ] minutes left..."
	let start=start+1
	sleep 60
done
echo "Done!"